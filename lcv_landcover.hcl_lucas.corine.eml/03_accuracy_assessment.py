import seaborn as sn
from matplotlib import pyplot as plt

def make_confusion_matrix(cm,
                          heatmap_type='standard',
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''
    cf = cm.copy()
    tp = np.zeros(cf.shape)
    
        
    if heatmap_type == 'predicted_rate':
        from matplotlib import colors
        true_positives = []
        total_predicted = np.sum(cf,axis=1)
        cf = (cf.T / total_predicted).T*100
        for i in range(cf.shape[1]):
            tp[i,i] = np.abs(cf[i,i])
        
        
    for i in range(tp.shape[0]):
        tp[i,i] = np.abs(cf[i,i])
    
    

    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names)==cf.size:
        group_labels = [f"{value}\n" for value in group_names]
    else:
        group_labels = blanks

    if count:
        if heatmap_type == 'standard':
            group_counts = [f"{abs(value):0.0f}\n" for value in cf.flatten()]
        if heatmap_type == 'predicted_rate':
            group_counts = [f"{abs(value):0.1f}\n" for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = [f"{abs(value):.2%}" for value in cf.flatten()/np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels,group_counts,group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0],cf.shape[1])


    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        #Accuracy is sum of diagonal divided by total observations
        accuracy  = np.trace(cf) / float(np.sum(cf))

        #if it is a binary confusion matrix, show some more stats
        if len(cf)==2:
            #Metrics for Binary Confusion Matrices
            precision = cf[1,1] / sum(cf[:,1])
            recall    = cf[1,1] / sum(cf[1,:])
            f1_score  = 2*precision*recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy,precision,recall,f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""


    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize==None:
        #Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks==False:
        #Do not show categories if xyticks is False
        categories=False


    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    
    if heatmap_type == 'predicted_rate':
        ax = sns.heatmap(cf,annot=box_labels,fmt="",cmap=plt.get_cmap("Reds"),cbar=cbar,xticklabels=categories,yticklabels=categories,square=True,norm=None)    
        ax = sns.heatmap(tp,annot=box_labels,fmt="",cmap=plt.get_cmap('Greens'),cbar=cbar,xticklabels=categories,yticklabels=categories,norm=None,square=True,mask=tp==0)
    
    if heatmap_type == 'standard':
        ax = sns.heatmap(cf,annot=box_labels,fmt="",cmap=cmap,cbar=cbar,xticklabels=categories,yticklabels=categories)
    
    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)
    
    if title:
        plt.title(title)
    plt.show()
cms = cms_mean
hcl = hcl_mean
lvl = 'clc3'
figsize=(30,25)
cbar = False

make_confusion_matrix(cms[lvl],cmap=plt.get_cmap('RdYlGn_r'),heatmap_type='predicted_rate',percent=False,count=True,cbar=cbar,figsize=figsize,categories=sorted(set(hcl[lvl])))
make_confusion_matrix(cms[lvl],cmap=plt.get_cmap('Blues'),heatmap_type='standard',percent=False,count=True,figsize=figsize,categories=sorted(set(hcl[lvl])))