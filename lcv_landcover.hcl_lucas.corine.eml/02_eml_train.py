import argparse
from datetime import datetime
import geopandas as gpd
import importlib
import joblib
import os.path as osp
import pandas as pd
import numpy as np
from sklearn.metrics import ConfusionMatrixDisplay, classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GroupKFold, KFold
import sys
import tensorflow as tf
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
import time
import xgboost as xgb


def build_ann_2(input_shape, output_shape, 
                n_layers = 3, n_neurons = 32, activation = 'relu',
                dropout_rate = 0.0, learning_rate = 0.0001,
                output_activation = 'softmax', loss = 'categorical_crossentropy'):
    try:
        from tensorflow.keras.layers import Dense, BatchNormalization, Dropout
        from tensorflow.keras.models import Sequential
        from tensorflow.keras.optimizers import Nadam
    
    except ImportError as e:
        warnings.warn('build_ann requires tensorflow>=2.5.0')

    model = Sequential()
    model.add(Dense(input_shape, activation=activation))

    for i in range(0, n_layers):
        model.add(Dense(n_neurons, activation=activation))
        model.add(Dropout(dropout_rate))
        model.add(BatchNormalization())

    model.add(Dense(output_shape, activation=output_activation))
    model.compile(loss=loss,optimizer=Nadam(learning_rate=learning_rate))
    return model


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('path_points') # '/mnt/atlas/landcover_v4/points_44_preprocessed.joblib'
    p.add_argument('--dir_output',default=None)
    p.add_argument('--cv_folds',default=5)
    p.add_argument('--cv_njobs',default=None)
    p.add_argument('--meta_pca',action='store_true')
    p.add_argument('--covariate_prefixes',default='lcv,dtm,clm,hyd')
    p.add_argument('--exclude_keywords',default='osm_')
    p.add_argument('--target_column',default='clc3_orig')
    p.add_argument('--weight_column',default='confidence')
    p.add_argument('--spatial_cv_column',default='tile_id')
    p.add_argument('--min_samples_per_class',default=0)
    p.add_argument('--dir_eumap',default="/mnt/diskstation/geoharmonizer/landcover_v4/eumap")
    a = p.parse_args()
    
    # Further calculate defaults and provided arguments     
    a.cv_njobs = a.cv_folds if a.cv_njobs is None else a.cv_njobs
    a.covariate_prefixes = a.covariate_prefixes.split(",")
    a.exclude_keywords = a.exclude_keywords.split(",")
    a.dir_output = osp.split(a.path_points)[0] if a.dir_output is None else a.dir_output
    
      
    n = datetime.now()
    timestamp = f"{n.year}.{n.month:02}.{n.day:02}"
    print(f"Training started on {timestamp} at {n.hour}:{n.minute}")
    name_points = osp.splitext(osp.split(a.path_points)[1])[0]
    name_landmapper = f"landmapper_{name_points}_{timestamp}"
    path_landmapper = osp.join(a.dir_output,name_landmapper+".joblib")
    print(f"Trained landmapper will be saved at {path_landmapper}")
    
    
    # Import eumap package from specified local directory
    path_eumap = osp.join(a.dir_eumap,'eumap/__init__.py')
    if osp.exists(path_eumap):
        spec = importlib.util.spec_from_file_location('eumap', path_eumap)
        module = importlib.util.module_from_spec(spec)
        sys.modules[spec.name] = module 
        spec.loader.exec_module(module)
    else:
        print("Could not find eumap locally in the specified directory, will import regularly")
    from eumap.mapper import LandMapper
    from eumap.mapper import build_ann
                          

    # Load training points
    pts = joblib.load(a.path_points)
    
    # Perform some last preprocessing steps 
    pts = pts.dropna(subset=[a.target_column])
    for kw in a.exclude_keywords:
        cols = [c for c in pts.columns if kw not in c]
        pts=pts[cols]

    # Count number of covariates
    covs = 0
    for c in pts.columns:
        for prfx in a.covariate_prefixes:
            if c.startswith(prfx):
                covs += 1

#     input_shape = (-1, covs)
    input_shape = covs
    n_classes = len(pd.unique(pts[a.target_column]))
    
    # Specify the three models and metalearner
    estimator_rf = RandomForestClassifier(
        n_estimators=85, 
        n_jobs=96, 
        class_weight=None, 
        max_depth=25, 
        max_features=0.5, 
        min_samples_leaf=20)
    
    estimator_bgtree = xgb.XGBClassifier(
        n_jobs=96, 
        alpha=0.48376656106914784, 
        eta=0.2817753692482472, 
        gamma=1, 
        max_depth=7, 
        n_estimators=28, 
        objective='multi:softmax', 
        tree_method='hist', 
        booster='gbtree')
    
    estimator_ann = Pipeline([
        ('standardize', StandardScaler()),
        ('estimator', KerasClassifier(build_ann_2, input_shape=input_shape, output_shape=n_classes, \
            epochs=50, batch_size=64, learning_rate = 0.0005, \
            dropout_rate=0.15, n_layers = 4, n_neurons=64, shuffle=True, verbose=1))], 
        verbose=True)

    meta_estimator = LogisticRegression(
        solver='saga', 
        multi_class='multinomial', 
        n_jobs=-1, 
        verbose=True)

    estimator_list = [estimator_rf, estimator_bgtree, estimator_ann]
    
    # Check if the spatial fold column is present in the training data
    if a.spatial_cv_column in pts.columns:
        cv_method = GroupKFold(a.cv_folds)
    else:
        print(f"Spatial cross-validation fold identifier {a.spatial_cv_column} was not found in the training data. Will use regular cross-validation")
        cv_method = KFold(a.cv_folds)
    
    if a.weight_column is not None:
        max_weig = np.max(pts[a.weight_column])
        pts[a.weight_column] = pts[a.weight_column] / max_weig

    # remove me
    print(np.min(pts[a.weight_column]), np.max(pts[a.weight_column]))

    m = LandMapper(points=pts, 
        feat_col_prfxs = a.covariate_prefixes, 
        target_col = a.target_column, 
        estimator_list = estimator_list, 
        meta_estimator = meta_estimator,
        meta_pca = a.meta_pca,
        min_samples_per_class = float(a.min_samples_per_class),
        cv = cv_method,
        cv_njobs=int(a.cv_njobs),
        pred_method='predict_proba',
        weight_col=a.weight_column,
        cv_group_col = a.spatial_cv_column,
        verbose = True)
    
    m.train()
    
    time.sleep(5)
    n = datetime.now()
    timestamp = f"{n.year}.{n.month:02}.{n.day:02}"
    print(f"Training completed on {timestamp} at {n.hour}:{n.minute}")
    
    m.save_instance(path_landmapper)
    print(f"Landmapper was succesfully saved at {path_landmapper}")

