source("vegetation_mapping_functions")

setwd("veg_mapping")

#### Parameters ####
species="Abies_alba"
model_type = "PNV"

years = c(2000, 2004, 2008, 2012, 2016, 2020)
dir.create(paste0(getwd(), "/05_production/"))
out.dir = paste0(getwd(), "/05_production/")
dir.create(out.dir)

dir.create(paste0(getwd(), "/rds"))
rds.dir=paste0(getwd(), "/rds")

## The "rds" folder will contain rds files with the spatiotemporal stacks of covariates at 30m used to predict a 30km tile
## Download them from zenodo

static.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_static.rds?download=1", 
                             tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_static.rds"))

y2000.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2000.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2000.rds"))

y2004.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2004.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2004.rds"))

y2008.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2008.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2008.rds"))

y2012.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2012.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2012.rds"))

y2016.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2016.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2016.rds"))

y2020.rds <- curl::curl_download("https://zenodo.org/record/5821865/files/tile_8766_30m_2020.rds?download=1", 
                                  tempfile()) %>%
  readRDS.gz() %>% 
  saveRDS.gz(paste0("rds/tile_8766_2020.rds"))

rm(static.rds, y2000.rds, y2004.rds, y2008.rds, y2012.rds, y2016.rds, y2020.rds)

model = readRDS.gz(paste0("04_optimized_models/", species, "_", model_type, "_eml.rds"))

#### mlr predictions for realized distribution ####

## Example input, with structure "TILE_ID.YEAR"
## TILE_ID is according to the 30km grid overlaid on Europe

tile_id = "8766"
input = apply(expand.grid(tile_id, years), 1, paste, collapse=".")

## Run the predictions ###
predict_tiles(input, model, model_type, rds.dir, out.dir, sd = T)

#### mlr predictions for potential distribution ####
model_type = "PNV"
years = c(2020)
model = readRDS.gz(paste0("04_optimized_models/", species, "_", model_type, "_eml.rds"))
input = apply(expand.grid(tile_id, years), 1, paste, collapse=".")

predict_tiles(input, model, model_type, rds.dir, out.dir, sd = T)


#### Check the function 'predict_tiles_S3' in the 'vegetation_mapping_functions' script
#### to see how we predict the tile using Amazon S3 storage servers