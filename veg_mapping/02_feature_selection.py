from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFECV, RFE
from sklearn.metrics import log_loss
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import GroupKFold
import datatable
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

specie = 'Abies_alba'
csv = '/02_datasets/'+specie+'_ANV_training.csv.gz'
data = datatable.fread(csv).to_pandas()

features[rfe.support_].nbytes / math.pow(1024,3)

groups = data[['Tile_ID']].astype('int').to_numpy()

target = data[['Atlas_class']].astype('int').to_numpy()

feat_cols = data.columns[data.columns.str.endswith("tif")]
feat_cols2 = feat_cols.append(data.columns[data.columns.str.endswith("tiff")])
features = data[feat_cols2].to_numpy()

total_samples = features.shape[0]
rand_samples = int(total_samples*0.25)
rand_idx = np.random.choice(total_samples, rand_samples, replace=False)

feature_sub = features[rand_idx,:]
target_sub = target[rand_idx].ravel()
groups_sub = groups[rand_idx]

def log_loss_scorer(clf, X, y_true):
    class_labels = clf.classes_
    y_pred_proba = clf.predict_proba(X)
    error = log_loss(y_true, y_pred_proba, labels=class_labels)
    return error * -1

rfecv = RFECV(estimator=RandomForestClassifier(50, n_jobs=50), cv=GroupKFold(5), step=20, min_features_to_select=10, n_jobs=5, scoring=log_loss_scorer)
rfecv.fit(feature_sub, target_sub, groups=groups_sub)

plt.figure(figsize=(12,6))
plt.title("RFECV for silver fir - realized distribution")
plt.xlabel("Number of Features Selected")
plt.ylabel("Logarithmic Loss based on 5-fold spatial cv")
plt.plot(range(rfecv.min_features_to_select, len(feat_cols2)+rfecv.step, rfecv.step), rfecv.grid_scores_*-1)
plt.xticks(range(rfecv.min_features_to_select, len(feat_cols2)+rfecv.step, rfecv.step))
plt.savefig('logloss.png')
plt.show()

rfe = RFE(estimator=RandomForestClassifier(50, n_jobs=50), step=10, n_features_to_select=154, verbose=1)
rfe.fit(feature_sub, target_sub)

rank = pd.DataFrame(rfe.ranking_, index=feat_cols2, columns=['Rank']).sort_values(by='Rank',ascending=True).to_csv(specie+'_PNV_cv.csv')

# Checking
feat_cols[rfe.support_]
pred_sub = cross_val_predict(RandomForestClassifier(50, n_jobs=50), feature_sub[:,rfe.support_], target_sub, cv=GroupKFold(5), groups=groups_sub, n_jobs=5, method='predict_proba')

log_loss(target_sub, pred_sub)