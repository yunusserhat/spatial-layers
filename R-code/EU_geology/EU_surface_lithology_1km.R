## EU surface geology map
## http://www.europe-geology.eu/onshore-geology/geological-map/onegeologyeurope/

library(rgdal)
library(foreign)
library(raster)
library(terra)
load(".RData")
xllcorner = 900000
xurcorner = 6540000
yllcorner = 930010
yurcorner = 5460010
cellsize = 250

geol = read.dbf("EGDI_GE_GeologicUnit_EN_1M_Surface_LithologyPolygon.dbf")
## 244848 x 6
xs = summary(geol$represent0, maxsum = length(levels(geol$represent0)))
xs <- tibble::rownames_to_column(data.frame(xs), "Count")
write.csv(xs, "geology_summary.csv")
geol$GEO_INT = as.integer(geol$represent0)
write.dbf(geol, "EGDI_GE_GeologicUnit_EN_1M_Surface_LithologyPolygon.dbf")
geol.leg = data.frame(NAMES=levels(geol$represent0), Value=1:length(levels(geol$represent0)))
write.csv(geol.leg, "geology_legend.csv")
system(paste('ogr2ogr -t_srs \"EPSG:3035\" geol_poly_a.shp EGDI_GE_GeologicUnit_EN_1M_Surface_LithologyPolygon.shp'))
#system('ogrinfo geol_poly_a.shp')
system(paste0('saga_cmd grid_gridding 0 -INPUT \"geol_poly_a.shp\" -FIELD \"GEO_INT\" -GRID \"geol_1km.sgrd\" -GRID_TYPE 3 -TARGET_DEFINITION 0 -TARGET_USER_SIZE ', cellsize, ' -TARGET_USER_XMIN ', xllcorner+cellsize/2,' -TARGET_USER_XMAX ', xurcorner-cellsize/2, ' -TARGET_USER_YMIN ', yllcorner+cellsize/2,' -TARGET_USER_YMAX ', yurcorner-cellsize/2))
unlink("EUSURFLITH_250m.tif")
system(paste('gdal_translate geol_1km.sdat EUSURFLITH_250m.tif -ot \"Int16\" -a_nodata \"32767\" -co \"COMPRESS=DEFLATE\"'))
## 22560 x 18120
raster::raster("EUSURFLITH_250m.tif")

## covariate layers:
dtm.lst = paste0("/mnt/EU_DATA/DTM/", c("dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3.tif", "dtm_slope_gedi.saga.gis_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif", "dtm_vbf_gedi.saga.gis_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif", "dtm_vdepth_gedi.saga.gis_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif", "dtm_devmean2_gedi.saga.gis_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"))
s2.lst = c(paste0("/mnt/EU_DATA/SENTINEL-2/swir2/P25/", c("s2l2a_ard_20181202_20190320_swir2_P25.tif", "s2l2a_ard_20171202_20180320_swir2_P25.tif", "s2l2a_ard_20191202_20200320_swir2_P25.tif")),
           paste0("/mnt/EU_DATA/SENTINEL-2/nir/P25/", c("s2l2a_ard_20171202_20180320_nir_P25.tif", "s2l2a_ard_20181202_20190320_nir_P25.tif", "s2l2a_ard_20191202_20200320_nir_P25.tif")))
x = parallel::mclapply(s2.lst, function(i){system(paste0('gdalwarp ', i, ' ', basename(i), ' -tr 250 250 -te 900000 930010 6540000 5460010 -r \"average\" -co \"COMPRESS=DEFLATE\"'))}, mc.cores=6)
x = parallel::mclapply(dtm.lst, function(i){system(paste0('gdalwarp ', i, ' ', basename(i), ' -tr 250 250 -te 900000 930010 6540000 5460010 -co \"COMPRESS=DEFLATE\"'))}, mc.cores=6)

## Filter out missing pixels in parent material and drainage class maps:
pm = raster("EUSURFLITH_250m.tif")
#rnd.pnts = sampleRandom(pm, size=12e4, na.rm=TRUE, sp=TRUE)
r = rast("EUSURFLITH_250m.tif")
cells = spatSample(r, 8e5, method="random", cells=TRUE)
rnd.pnts <- r[cells]
xy <- xyFromCell(r, cells)
rnd.pnts = cbind(xy, rnd.pnts)
sel = !is.na(rnd.pnts[,3])
summary(sel)
## 128943
sel.tifs = c("EUSURFLITH_250m.tif", basename(dtm.lst), basename(s2.lst))
pnts.v = terra::vect(rnd.pnts[sel,1:2], crs="EPSG:3035")
ov.lst = parallel::mclapply(sel.tifs, function(i){terra::extract(terra::rast(i), pnts.v)}, mc.cores=length(sel.tifs))
ov.df = dplyr::bind_cols(lapply(ov.lst, function(i){i[,2]}))
names(ov.df) = tools::file_path_sans_ext(basename(sel.tifs))
ov.df$EUSURFLITH_250m = as.factor(rnd.pnts[sel,3])

library(ranger)
fm.pm <- as.formula(paste('EUSURFLITH_250m ~ ', paste(tools::file_path_sans_ext(basename(sel.tifs)[-1]), collapse = "+")))
rf.pm <- ranger(fm.pm, data=ov.df[complete.cases(ov.df[,all.vars(fm.pm)]),], importance="impurity", write.forest = TRUE, num.trees = 85, mtry=4)
rf.pm ## 40% accuracy
rf.pm$variable.importance
saveRDS(rf.pm, file="RF_EUSURFLITH_250m.rds")
save.image()

## Predict values
unlink("lcv_landcover.12_250m.tif")
system(paste0('gdalwarp /mnt/diskstation/eu_data/Land_mask/lcv_landcover.12_pflugmacher2019_c_1m_s0..0m_2014..2016_eumap_epsg3035_v0.1.tif lcv_landcover.12_250m.tif -tr 250 250 -te 900000 930010 6540000 5460010 -r \"mode\" -co \"COMPRESS=DEFLATE\"'))
library(landmap)
obj = GDALinfo("lcv_landcover.12_250m.tif")
tile.tbl = getSpatialTiles(obj, block.x=2e5)
## 667
#eumap = as(eumap, "SpatialPixelsDataFrame")
#eumap$EUSURFLITH_250m = readGDAL("EUSURFLITH_250m.tif")$band1[eumap@grid.index]
x = parallel::mclapply(1:nrow(tile.tbl), function(i){make_geo_tiles(i, tile.tbl, out.path="./tiled", tif1="lcv_landcover.12_250m.tif", sel.tifs, rf.pm)}, mc.cores=78)
t.lst <- list.files("./tiled", pattern=".tif", full.names=TRUE)
out.tmp <- "t_list.txt"
vrt.tmp <- "geo.vrt"
cat(t.lst, sep="\n", file=out.tmp)
system(paste0('gdalbuildvrt -input_file_list ', out.tmp, ' ', vrt.tmp))

## Indicator maps ----
eugeo = readGDAL("geo.vrt")
eugeo$band1 = ifelse(is.na(eugeo$band1), 0, eugeo$band1)
for(i in 1:nrow(geol.leg)){
  filename <- strsplit(geol.leg$NAMES[i], "http://inspire.ec.europa.eu/codelist/LithologyValue/")[[1]][2]
  eugeo@data[,2] <- ifelse(eugeo@data[,1]==geol.leg$Value[i], 100, 0)
  writeGDAL(eugeo[2], paste0("./filtered250m/dtm_surface.lithology_egdi.1m.", tolower(filename), "_p_250m_2018_eumap_epsg3035_v1.0.tif"), type="Byte", options="COMPRESS=DEFLATE", mvFlag=255)
  gc()
}
file.copy("EUSURFLITH_250m.tif", "./filtered250m/dtm_surface.lithology_egdi.1m_c_250m_2018_eumap_epsg3035_v1.0.tif")
file.copy("geology_legend.csv", "./filtered250m_cog/dtm_surface.lithology_egdi.1m_c_250m_2018_eumap_epsg3035_v1.0.tif.csv")
all.tifs = list.files("./filtered250m", glob2rx("*.tif$"), full.names = TRUE)
## convert to COG ----
x = parallel::mclapply(all.tifs, function(i){ out.tif <- gsub("/filtered250m/", "/filtered250m_cog/", i); if(!file.exists(out.tif)) system(paste0('gdal_translate ', i, ' ', out.tif, ' -co COMPRESS=DEFLATE -co LEVEL=9 -of COG'))}, mc.cores=78)

rm(eugeo)
gc()
save.image()
