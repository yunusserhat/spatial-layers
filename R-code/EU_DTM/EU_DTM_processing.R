## Derivation of DTM parameters for EU
## tom.hengl@opengeohub.org

library(rgdal)
library(fastSave)
load(".RData")
source('DTM_functions.R')
source('/mnt/GeoHarmonizer/landmask_30m/code/GeoHarmonizer_functions.R')

## catchment area log
ca.tifs = list.files(path="./tt", pattern=glob2rx("dtm_*_catchm.tif$"), full.names=TRUE, recursive=TRUE)
log_catchm = function(i){
  out = gsub("_catchm.tif", "_log.catchm.tif", i)
  if(!file.exists(out)){
    x = readGDAL(i)
    x$band2 = log10(x$band1+1)*10
    writeGDAL(x["band2"], out, type="Byte", mvFlag = 255, options=c("COMPRESS=DEFLATE"))
  }
}
#log_catchm(ca.tifs[100])
x = mclapply(ca.tifs, log_catchm, mc.cores = 80)

## mosaick:
#tv.lst = c("twi", "log.catchm", "vbf", "out")
tv.lst = c("vdepth", "vbf", "out")
tifs.lst = paste0("/mnt/GeoHarmonizer/EU_DTM/dtm_", c("vertical.depth", "mrvbf", "elev.hydrocorrect"), "_gedi.", c("saga.gis", "saga.gis", "whitebox"), "_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif")
mosaic_30m = function(j, out.tif){
  if(!file.exists(out.tif)){
    x = list.files("./tt", pattern=glob2rx(paste0("dtm_*_", j, ".tif$")), recursive = TRUE, full.names = TRUE)
    mosaic_eu(x, out.tif, ot="Int16", dstnodata=-32768)
  }
}
x = parallel::mclapply(1:length(tv.lst), function(i){mosaic_30m(tv.lst[i], tifs.lst[i])}, mc.cores = length(tv.lst))

## 50m derivatives:
system('gdalwarp -r "average" -of GTiff dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./50m/dtm_elev.lowestmode_gedi.eml_m_50m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -tr 50 50 -multi -wo \"NUM_THREADS=ALL_CPUS\" -co \"BIGTIFF=YES\"') ## -of \"SAGA\" -dstnodata \"-32767\"
## SAGA GIS format
system('gdalwarp -r "average" -of GTiff dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat -tr 100 100 -multi -wo \"NUM_THREADS=ALL_CPUS\" -co \"BIGTIFF=YES\" -of \"SAGA\" -dstnodata \"-32767\"')

## 100m derivatives ----
saga_DEM_derivatives(INPUT="./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sgrd", sel=c("CRV","VBF","OPN","DVM","MRN","VDP","TPI"))
sdat.lst = list.files("./100m", pattern=glob2rx("*_v0.2_*.sdat$"), full.names = TRUE)
x = parallel::mclapply(sdat.lst, sdat2geotif, mc.cores = 6)
## hillshade
system(paste0('gdaldem hillshade ./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat ./100m/dtm_hillshade.multid_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -multidirectional -of GTiff -b 1 -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"')) ## -z 1.0 -s 1.0 -az 315.0 -alt 45.0
## slope map
system(paste0('gdaldem slope ./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat ./100m/dtm_slope.percent_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -p -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
system(paste0('gdaldem TRI ./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat ./100m/dtm_tri_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
tif.100m = list.files("./100m", pattern=glob2rx("*_v0.2_*.tif$"), full.names = TRUE)
vrb.lst = sapply(tif.100m, function(i){strsplit(basename(i), "_")[[1]][11]})
vrb.lst = sapply(vrb.lst, function(i){strsplit(i, "\\.")[[1]][1]})
ntif.100m = paste0("/mnt/EU_DATA/DTM/dtm_", vrb.lst, "_gedi.saga.gis_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif")
x = parallel::mclapply(1:length(tif.100m), function(i){system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=8 -co LEVEL=9 -a_srs \"EPSG:3035\" -of COG ', tif.100m[i],' ', ntif.100m[i]))}, mc.cores=length(tif.100m))

## https://grass.osgeo.org/grass78/manuals/addons/r.northerness.easterness.html
# GRASS 7.8.5 (EU_DTM):~ > g.region -a raster=dtm_elev align=dtm_elev
# GRASS 7.8.5 (EU_DTM):~ > r.northerness.easterness elevation=dtm_elev --overwrite
library(rgrass7)
rname <- "./100m/dtm_elev.lowestmode_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat"
# Set GRASS environment and database location 
loc <- initGRASS("/usr/lib/grass78", home="/data/tmp/", 
                 gisDbase="GRASS_TEMP", override=TRUE)
execGRASS("r.in.gdal", flags="o", parameters=list(input=rname, output="mDLSM"))
execGRASS("g.region", parameters=list(raster="mDLSM"))
execGRASS("r.northerness.easterness", parameters=list(elevation="mDLSM"))
execGRASS("r.out.gdal", parameters=list(input="mDLSMg",
                                        output="./100m/dtm_northness_gedi.eml_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif", 
                                        type="Int16", createopt="COMPRESS=DEFLATE"))
## clean-up
unlink("./GRASS_TEMP", recursive = TRUE)
unset.GIS_LOCK()
unlink_.gislock()
remove_GISRC()
## Export to COG:
#system('gdalwarp /data/EU_DTM/100m/dtm_northness.slope_gedi.grass7_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif /data/EU_DTM/30m/dtm_northness.slope100m_gedi.grass7_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -tr 30 30')
imin = -70; imax = 70; omin = 0; omax=140; ot = "Byte"; mv = 255
system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=64 -co LEVEL=9 -of COG /data/EU_DTM/100m/dtm_northness.slope_gedi.grass7_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif /mnt/EU_DATA/DTM/dtm_northness.slope_gedi.grass7_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -scale ', imin, ' ', imax, ' ', omin, ' ', omax, ' -ot \"', ot, '\" -a_nodata \"', mv, '\"'))
imin = 0; imax = 20; omin = 0; omax=200; ot = "Byte"; mv = 255
system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=64 -co LEVEL=9 -of COG /data/EU_DTM/50m/dtm_topidx_gedi.eml_m_50m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif /mnt/EU_DATA/DTM/dtm_topidx_gedi.eml_m_50m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -scale ', imin, ' ', imax, ' ', omin, ' ', omax, ' -ot \"', ot, '\" -a_nodata \"', mv, '\"'))

## 250m derivatives:
system('gdalwarp -r "average" -of GTiff dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./250m/dtm_elev.lowestmode_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat -tr 250 250 -multi -wo \"NUM_THREADS=ALL_CPUS\" -co \"BIGTIFF=YES\" -of \"SAGA\" -dstnodata \"-32767\"')
saga_DEM_derivatives(INPUT="./250m/dtm_elev.lowestmode_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sgrd", sel=c("CRV","VBF","OPN","DVM","MRN","VDP","TPI"))
sdat.lst = list.files("./250m", pattern=glob2rx("*_v0.2_*.sdat$"), full.names = TRUE)
x = parallel::mclapply(sdat.lst, sdat2geotif, mc.cores = 6)
## slope map
system(paste0('gdaldem slope ./250m/dtm_elev.lowestmode_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat ./250m/dtm_slope.percent_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -p -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
system(paste0('gdaldem TRI ./250m/dtm_elev.lowestmode_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat ./250m/dtm_tri_gedi.eml_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
tif.250m = list.files("./250m", pattern=glob2rx("*_v0.2_*.tif$"), full.names = TRUE)
vrb.lst = sapply(tif.250m, function(i){strsplit(basename(i), "_")[[1]][11]})
vrb.lst = sapply(vrb.lst, function(i){strsplit(i, "\\.")[[1]][1]})
ntif.250m = paste0("/mnt/EU_DATA/DTM/dtm_", vrb.lst, "_gedi.saga.gis_m_250m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif")
x = parallel::mclapply(1:length(tif.250m), function(i){system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=8 -co LEVEL=9 -of COG ', tif.250m[i],' ', ntif.250m[i]))}, mc.cores=length(tif.250m))

#system(paste('gdal_translate /mnt/GeoHarmonizer/EU_DTM/dtm_elev.hydrocorrect_gedi.whitebox_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./30m/dtm_elev.hydrocorrect_gedi.whitebox_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat -of \"SAGA\"'))
system(paste('gdal_translate dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./30m/dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sdat -of \"SAGA\" -a_nodata \"-32767\"'))
## all other derivatives:
saga_DEM_derivatives(INPUT="./30m/dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.sgrd", sel=c("CRV","OPN","DVM","MRN"))
sdat.lst = list.files("./30m", pattern=glob2rx("*_v0.2_*.sdat$"), full.names = TRUE)
x = parallel::mclapply(sdat.lst, sdat2geotif, mc.cores = 4)
## hillshade
#unlink("/data/EU_DTM/30mCO/height_terrain_hillshade_2019_30m.tif")
system(paste0('gdaldem hillshade dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./30m/dtm_hillshade.multid_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -multidirectional -of GTiff -b 1 -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"')) ## -z 1.0 -s 1.0 -az 315.0 -alt 45.0
## slope map
system(paste0('gdaldem slope dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./30m/dtm_slope.percent_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -p -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
system(paste0('gdaldem TRI dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif ./30m/dtm_tri_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -compute_edges -of GTiff -co \"BIGTIFF=YES\" -co NUM_THREADS=\"ALL_CPUS\" -co \"COMPRESS=LZW\"'))
#system(paste('gdal_translate ./30m/Aspect.sdat ./30m/dtm_aspect_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -ot \"Int16\" -co \"BIGTIFF=YES\" -co \"COMPRESS=LZW\"'))
tif.30m = list.files("./30m", pattern=glob2rx("*.tif$"), full.names = TRUE)
## manual set
ntif.30m = paste0("/mnt/EU_DATA/DTM/dtm_", c(paste0(c("easterness"), "_gedi.grass7_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"), paste0(c("openn", "openp"), "_gedi.saga.gis_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"), paste0(c("hillshade.multid"), "_gedi.gdal_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"), paste0(c("northness"), "_gedi.grass7_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif"), paste0(c("slope.percent", "tri"), "_gedi.gdal_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif")))
x = parallel::mclapply(1:length(tif.30m), function(i){system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=8 -co LEVEL=9 -a_srs \"EPSG:3035\" -of COG ', tif.30m[i],' ', ntif.30m[i]))}, mc.cores=length(tif.30m))
imin = 0; imax = 100; omin = 0; omax=200; ot = "Byte"; mv = 255
system(paste0('gdal_translate --config GDAL_CACHEMAX 9216 -co BLOCKSIZE=1024 -co BIGTIFF=YES -co COMPRESS=DEFLATE -co NUM_THREADS=64 -co LEVEL=9 -of COG ./30m/dtm_slope.percent_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif /data/EU_DATA/DTM/dtm_slope.percent_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif -scale ', imin, ' ', imax, ' ', omin, ' ', omax, ' -ot \"', ot, '\" -a_nodata \"', mv, '\"'))


save.image.pigz(n.cores = parallel::detectCores())