<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0" xmlns:gml="http://www.opengis.net/gml" xmlns:sld="http://www.opengis.net/sld">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>aq_pm25_pixel_count_1km_na_2018.08.01..2018.08.31_eumap_epsg3035_v0.1prebeta</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry color="#f7fbff" quantity="0" label="0"/>
              <sld:ColorMapEntry color="#deebf7" quantity="4" label="4"/>
              <sld:ColorMapEntry color="#c6dbef" quantity="8" label="8"/>
              <sld:ColorMapEntry color="#9ecae1" quantity="12" label="12"/>
              <sld:ColorMapEntry color="#6baed6" quantity="16" label="16"/>
              <sld:ColorMapEntry color="#4292c6" quantity="20" label="20"/>
              <sld:ColorMapEntry color="#2171b5" quantity="24" label="24"/>
              <sld:ColorMapEntry color="#08519c" quantity="28" label="28"/>
              <sld:ColorMapEntry color="#08306b" quantity="31" label="31"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
