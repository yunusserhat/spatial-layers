<ns0:StyledLayerDescriptor xmlns:ns0="http://www.opengis.net/sld" xmlns:ns1="http://www.opengis.net/ogc" version="1.0.0">
  <ns0:UserLayer>
    <ns0:LayerFeatureConstraints>
      <ns0:FeatureTypeConstraint />
    </ns0:LayerFeatureConstraints>
    <ns0:UserStyle>
      <ns0:Name>GRASS color table</ns0:Name>
      <ns0:Title>Precipitation mm x 10</ns0:Title>
      <ns0:FeatureTypeStyle>
        <ns0:Name />
        <ns0:Rule>
          <ns0:RasterSymbolizer>
            <ns0:Geometry>
              <ns1:PropertyName>grid</ns1:PropertyName>
            </ns0:Geometry>
            <ns0:Opacity>1</ns0:Opacity>
            <ns0:ColorMap>
              <ns0:ColorMapEntry color="#ffffff" label="0" opacity="1.0" quantity="0" />
              <ns0:ColorMapEntry color="#9effde" label="1" opacity="1.0" quantity="10" />
              <ns0:ColorMapEntry color="#c8ffff" label="5" opacity="1.0" quantity="50" />
              <ns0:ColorMapEntry color="#00ffff" label="10" opacity="1.0" quantity="100" />
              <ns0:ColorMapEntry color="#0000ff" label="20" opacity="1.0" quantity="200" />
              <ns0:ColorMapEntry color="#9933ff" label="30" opacity="1.0" quantity="300" />
              <ns0:ColorMapEntry color="#8000ff" label="40" opacity="1.0" quantity="400" />
              <ns0:ColorMapEntry color="#330033" label="50" opacity="1.0" quantity="500" />
              <ns0:ColorMapEntry color="#140014" label="70" opacity="1.0" quantity="700" />
              <ns0:ColorMapEntry color="#000000" label="100" opacity="1.0" quantity="1000" />              
              <ns0:ColorMapEntry color="#000000" label="1000" opacity="1.0" quantity="100000" />
            </ns0:ColorMap>
          </ns0:RasterSymbolizer>
        </ns0:Rule>
      </ns0:FeatureTypeStyle>
    </ns0:UserStyle>
  </ns0:UserLayer>
</ns0:StyledLayerDescriptor>
