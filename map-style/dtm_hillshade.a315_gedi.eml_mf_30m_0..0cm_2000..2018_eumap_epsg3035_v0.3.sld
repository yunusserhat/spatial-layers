<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_hillshade.a315_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3</sld:Name>
            <sld:Description>DTM hillshading</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#050505" label="145" opacity="1.0" quantity="145"/>
                            <sld:ColorMapEntry color="#2e2e2e" label="163" opacity="1.0" quantity="163.33333333333334"/>
                            <sld:ColorMapEntry color="#575757" label="182" opacity="1.0" quantity="181.66666666666666"/>
                            <sld:ColorMapEntry color="#7f7f7f" label="200" opacity="1.0" quantity="200"/>
                            <sld:ColorMapEntry color="#a8a8a8" label="218" opacity="1.0" quantity="218.33333333333331"/>
                            <sld:ColorMapEntry color="#d1d1d1" label="237" opacity="1.0" quantity="236.66666666666666"/>
                            <sld:ColorMapEntry color="#fafafa" label="255" opacity="1.0" quantity="255"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
