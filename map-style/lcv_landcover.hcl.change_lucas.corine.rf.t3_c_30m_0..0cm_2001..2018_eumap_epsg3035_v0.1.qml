
<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" version="3.10.4-A Coruña" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer band="1" opacity="1" type="paletted" alphaBand="-1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
      <paletteEntry color="#f294fb" label="Crop expansion" alpha="255" value="1"/>
        <paletteEntry color="#a7562c" label="Deforestation" alpha="255" value="2"/>
        <paletteEntry color="#965591" label="Deforestation &amp; crop expansion" alpha="255" value="3"/>
        <paletteEntry color="#cb0809" label="Deforestation &amp; urbanization" alpha="255" value="4"/>
        <paletteEntry color="#c7c7c7" label="Desertification" alpha="255" value="5"/>
        <paletteEntry color="#ffba3b" label="Land abandonment" alpha="255" value="6"/>
        <paletteEntry color="#d1ad67" label="Land abandonment &amp; desertification" alpha="255" value="7"/>
        <paletteEntry color="#6e6e6e" label="Other" alpha="255" value="8"/>
        <paletteEntry color="#008626" label="Reforestation" alpha="255" value="9"/>
        <paletteEntry color="#ff0d0e" label="Urbanization" alpha="255" value="10"/>
        <paletteEntry color="#0033c3" label="Water expansion" alpha="255" value="11"/>
        <paletteEntry color="#ff6565" label="Water reduction" alpha="255" value="12"/>
        <paletteEntry color="#c98b82" label="Wetland degr. &amp; Desertification" alpha="255" value="13"/>
        <paletteEntry color="#a385be" label="Wetland degr. &amp; crop expansion" alpha="255" value="14"/>
        <paletteEntry color="#ff6565" label="Wetland degr. &amp; urbanization" alpha="255" value="15"/>
        <paletteEntry color="#009082" label="Wetland degradation" alpha="255" value="16"/>
         
      </colorPalette>
      <colorramp name="[source]" type="randomcolors"/>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeRed="255" colorizeOn="0" colorizeBlue="128" saturation="0" grayscaleMode="0" colorizeGreen="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
