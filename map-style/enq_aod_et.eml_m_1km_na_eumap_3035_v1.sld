<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" xmlns:sld="http://www.opengis.net/sld">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>enq_aod_et.eml_m_1km_na_2019.04_eumap_3035_v1.0</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry color="#2b83ba" label="0" quantity="0"/>
              <sld:ColorMapEntry color="#4f9db4" label="0.025" quantity="0.025000000000000001"/>
              <sld:ColorMapEntry color="#74b7ae" label="0.05" quantity="0.050000000000000003"/>
              <sld:ColorMapEntry color="#99d0a7" label="0.075" quantity="0.074999999999999997"/>
              <sld:ColorMapEntry color="#b7e2a8" label="0.1" quantity="0.10000000000000001"/>
              <sld:ColorMapEntry color="#cfecb0" label="0.125" quantity="0.125"/>
              <sld:ColorMapEntry color="#e7f6b8" label="0.15" quantity="0.14999999999999999"/>
              <sld:ColorMapEntry color="#ffffbf" label="0.175" quantity="0.17499999999999999"/>
              <sld:ColorMapEntry color="#ffe8a4" label="0.2" quantity="0.20000000000000001"/>
              <sld:ColorMapEntry color="#fed189" label="0.225" quantity="0.22500000000000001"/>
              <sld:ColorMapEntry color="#feba6e" label="0.25" quantity="0.25"/>
              <sld:ColorMapEntry color="#f89957" label="0.275" quantity="0.27500000000000002"/>
              <sld:ColorMapEntry color="#ed6e43" label="0.3" quantity="0.29999999999999999"/>
              <sld:ColorMapEntry color="#e2432f" label="0.325" quantity="0.32500000000000001"/>
              <sld:ColorMapEntry color="#d7191c" label="0.35" quantity="0.34999999999999998"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
