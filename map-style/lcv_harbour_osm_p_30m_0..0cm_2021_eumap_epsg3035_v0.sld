<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>ifr_harbour_osm_p_30m_0..0cm_2021_eumap_epsg3035_v0</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#f2f0f0" label="1" quantity="1" opacity="1.0"/>
              <sld:ColorMapEntry color="#efe7e7" label="25" quantity="25" opacity="1.0"/>
              <sld:ColorMapEntry color="#ecdede" label="50" quantity="50" opacity="1.0"/>
              <sld:ColorMapEntry color="#e9d5d5" label="75" quantity="75" opacity="1.0"/>
              <sld:ColorMapEntry color="#e6cccc" label="100" quantity="100" opacity="1.0"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
