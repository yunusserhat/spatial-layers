<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>ifr_building_copernicus.osm_p_30m_0..0cm_2018..2021_eumap_epsg3035_v0.1</sld:Name>
            <sld:Description>Generated by SLD4raster - https://cbsuygulama.wordpress.com/sld4raster</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#f2c2c3" quantity="1" label="&lt; 25% (OSM)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#cd6669" quantity="75" label="25% - 75% (OSM)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#cd0006" quantity="100" label="75% - 100% (OSM)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#0020a0" quantity="101" label="75% - 100% (Copernicus)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#3f53a0" quantity="125" label="25% - 75% (Copernicus)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#dadff2" quantity="175" label="&lt; 25% (Copernicus)" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
