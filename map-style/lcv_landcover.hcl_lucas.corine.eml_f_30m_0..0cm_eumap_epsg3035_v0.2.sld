<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.hcl_lucas.corine.eml_f_30m_0..0cm_2020_eumap_epsg3035_v0.2</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#e6004d" quantity="1" label="1 - 111 - Continuous urban fabric"/>
              <sld:ColorMapEntry color="#ff0000" quantity="2" label="2 - 112 - Discontinuous urban fabric"/>
              <sld:ColorMapEntry color="#cc4df2" quantity="3" label="3 - 121 - Industrial or commercial units"/>
              <sld:ColorMapEntry color="#cc0000" quantity="4" label="4 - 122 - Road and rail networks and associated land"/>
              <sld:ColorMapEntry color="#e6cccc" quantity="5" label="5 - 123 - Port areas"/>
              <sld:ColorMapEntry color="#e6cce6" quantity="6" label="6 - 124 - Airports"/>
              <sld:ColorMapEntry color="#a600cc" quantity="7" label="7 - 131 - Mineral extraction sites"/>
              <sld:ColorMapEntry color="#a64d00" quantity="8" label="8 - 132 - Dump sites"/>
              <sld:ColorMapEntry color="#ff4dff" quantity="9" label="9 - 133 - Construction sites"/>
              <sld:ColorMapEntry color="#ffa6ff" quantity="10" label="10 - 141 - Green urban areas"/>
              <sld:ColorMapEntry color="#ffe6ff" quantity="11" label="11 - 142 - Sport and leisure facilities"/>
              <sld:ColorMapEntry color="#ffffa8" quantity="12" label="12 - 211 - Non-irrigated arable land"/>
              <sld:ColorMapEntry color="#ffff00" quantity="13" label="13 - 212 - Permanently irrigated land"/>
              <sld:ColorMapEntry color="#e6e600" quantity="14" label="14 - 213 - Rice fields"/>
              <sld:ColorMapEntry color="#e68000" quantity="15" label="15 - 221 - Vineyards"/>
              <sld:ColorMapEntry color="#f2a64d" quantity="16" label="16 - 222 - Fruit trees and berry plantations"/>
              <sld:ColorMapEntry color="#e6a600" quantity="17" label="17 - 223 - Olive groves"/>
              <sld:ColorMapEntry color="#e6e64d" quantity="18" label="18 - 231 - Pastures"/>
              <sld:ColorMapEntry color="#ffe6a6" quantity="19" label="19 - 241 - Annual crops associated with permanent crops"/>
              <sld:ColorMapEntry color="#ffe64d" quantity="20" label="20 - 242 - Complex cultivation patterns"/>
              <sld:ColorMapEntry color="#e6cc4d" quantity="21" label="21 - 243 - Land principally occupied by agriculture, with significant areas of natural vegetation"/>
              <sld:ColorMapEntry color="#f2cca6" quantity="22" label="22 - 244 - Agro-forestry areas"/>
              <sld:ColorMapEntry color="#80ff00" quantity="23" label="23 - 311 - Broad-leaved forest"/>
              <sld:ColorMapEntry color="#00a600" quantity="24" label="24 - 312 - Coniferous forest"/>
              <sld:ColorMapEntry color="#4dff00" quantity="25" label="25 - 313 - Mixed forest"/>
              <sld:ColorMapEntry color="#ccf24d" quantity="26" label="26 - 321 - Natural grasslands"/>
              <sld:ColorMapEntry color="#a6ff80" quantity="27" label="27 - 322 - Moors and heathland"/>
              <sld:ColorMapEntry color="#a6e64d" quantity="28" label="28 - 323 - Sclerophyllous vegetation"/>
              <sld:ColorMapEntry color="#a6f200" quantity="29" label="29 - 324 - Transitional woodland-shrub"/>
              <sld:ColorMapEntry color="#e6e6e6" quantity="30" label="30 - 331 - Beaches, dunes, sands"/>
              <sld:ColorMapEntry color="#cccccc" quantity="31" label="31 - 332 - Bare rocks"/>
              <sld:ColorMapEntry color="#ccffcc" quantity="32" label="32 - 333 - Sparsely vegetated areas"/>
              <sld:ColorMapEntry color="#000000" quantity="33" label="33 - 334 - Burnt areas"/>
              <sld:ColorMapEntry color="#a6e6cc" quantity="34" label="34 - 335 - Glaciers and perpetual snow"/>
              <sld:ColorMapEntry color="#a6a6ff" quantity="35" label="35 - 411 - Inland marshes"/>
              <sld:ColorMapEntry color="#4d4dff" quantity="36" label="36 - 412 - Peat bogs"/>
              <sld:ColorMapEntry color="#ccccff" quantity="37" label="37 - 421 - Salt marshes"/>
              <sld:ColorMapEntry color="#e6e6ff" quantity="38" label="38 - 422 - Salines"/>
              <sld:ColorMapEntry color="#a6a6e6" quantity="39" label="39 - 423 - Intertidal flats"/>
              <sld:ColorMapEntry color="#00ccf2" quantity="40" label="40 - 511 - Water courses"/>
              <sld:ColorMapEntry color="#80f2e6" quantity="41" label="41 - 512 - Water bodies"/>
              <sld:ColorMapEntry color="#00ffa6" quantity="42" label="42 - 521 - Coastal lagoons"/>
              <sld:ColorMapEntry color="#a6ffe6" quantity="43" label="43 - 522 - Estuaries"/>
              <sld:ColorMapEntry color="#e6f2ff" quantity="48" label="48 - 523 - Sea and ocean"/>
              <sld:ColorMapEntry color="#000000" quantity="49" label="49 - 999 - NODATA"/>
              <sld:ColorMapEntry color="#ffffff" quantity="50" label="50 - 990 - UNCLASSIFIED LAND SURFACE"/>
              <sld:ColorMapEntry color="#e6f2ff" quantity="255" label="255 - 995 - UNCLASSIFIED WATER BODIES"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
