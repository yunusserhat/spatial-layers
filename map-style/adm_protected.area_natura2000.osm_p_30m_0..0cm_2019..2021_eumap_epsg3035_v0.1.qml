<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#00244a"/>
        <prop k="preset_color_name_0" v="OSM (IUCN Ia)"/>
        <prop k="preset_color_1" v="#1565c0"/>
        <prop k="preset_color_name_1" v="OSM (IUCN Ib)"/>
        <prop k="preset_color_2" v="#1976d2"/>
        <prop k="preset_color_name_2" v="OSM (IUCN 2)"/>
        <prop k="preset_color_3" v="#1e88e5"/>
        <prop k="preset_color_name_3" v="OSM (IUCN 3)"/>
        <prop k="preset_color_4" v="#42a5f5"/>
        <prop k="preset_color_name_4" v="OSM (IUCN 4)"/>
        <prop k="preset_color_5" v="#64b5f6"/>
        <prop k="preset_color_name_5" v="OSM (IUCN 5)"/>
        <prop k="preset_color_6" v="#90caf9"/>
        <prop k="preset_color_name_6" v="OSM (IUCN 6)"/>
        <prop k="preset_color_7" v="#c000e6"/>
        <prop k="preset_color_name_7" v="OSM (Others)"/>
        <prop k="preset_color_8" v="#d4b139"/>
        <prop k="preset_color_name_8" v="Natura2000 (A)"/>
        <prop k="preset_color_9" v="#b9870c"/>
        <prop k="preset_color_name_9" v="Natura2000 (B)"/>
        <prop k="preset_color_10" v="#9a6716"/>
        <prop k="preset_color_name_10" v="Natura2000 (C)"/>
        <prop k="preset_color_11" v="#cf4100"/>
        <prop k="preset_color_name_11" v="Protected areas overlap"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="OSM (IUCN Ia)" color="#00244a" value="1"/>
        <item alpha="255" label="OSM (IUCN Ib)" color="#1565c0" value="20"/>
        <item alpha="255" label="OSM (IUCN 2)" color="#1976d2" value="30"/>
        <item alpha="255" label="OSM (IUCN 3)" color="#1e88e5" value="40"/>
        <item alpha="255" label="OSM (IUCN 4)" color="#42a5f5" value="50"/>
        <item alpha="255" label="OSM (IUCN 5)" color="#64b5f6" value="60"/>
        <item alpha="255" label="OSM (IUCN 6)" color="#90caf9" value="70"/>
        <item alpha="255" label="OSM (Others)" color="#c000e6" value="80"/>
        <item alpha="255" label="Natura2000 (A)" color="#d4b139" value="90"/>
        <item alpha="255" label="Natura2000 (B)" color="#b9870c" value="100"/>
        <item alpha="255" label="Natura2000 (C)" color="#9a6716" value="110"/>
        <item alpha="255" label="Protected areas overlap" color="#cf4100" value="120"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

