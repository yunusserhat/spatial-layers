<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#ebf3e9"/>
        <prop k="preset_color_name_0" v="10"/>
        <prop k="preset_color_1" v="#d0f6ae"/>
        <prop k="preset_color_name_1" v="20"/>
        <prop k="preset_color_2" v="#b5f974"/>
        <prop k="preset_color_name_2" v="30"/>
        <prop k="preset_color_3" v="#9afc3a"/>
        <prop k="preset_color_name_3" v="40"/>
        <prop k="preset_color_4" v="#80ff00"/>
        <prop k="preset_color_name_4" v="50"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="10" color="#ebf3e9" value="10"/>
        <item alpha="255" label="20" color="#d0f6ae" value="20"/>
        <item alpha="255" label="30" color="#b5f974" value="30"/>
        <item alpha="255" label="40" color="#9afc3a" value="40"/>
        <item alpha="255" label="50" color="#80ff00" value="50"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

