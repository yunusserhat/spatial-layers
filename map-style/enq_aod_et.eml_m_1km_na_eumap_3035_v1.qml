<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" hasScaleBasedVisibilityFlag="0" version="3.14.15-Pi" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>0</Searchable>
  </flags>
  <temporal enabled="0" fetchMode="0" mode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer opacity="1" band="1" classificationMax="0.35" type="singlebandpseudocolor" nodataColor="" alphaBand="-1" classificationMin="0">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" minimumValue="0" clip="0" classificationMode="2" maximumValue="0.35">
          <colorramp name="[source]" type="gradient">
            <prop k="color1" v="43,131,186,255"/>
            <prop k="color2" v="215,25,28,255"/>
            <prop k="discrete" v="0"/>
            <prop k="rampType" v="gradient"/>
            <prop k="stops" v="0.25;171,221,164,255:0.5;255,255,191,255:0.75;253,174,97,255"/>
          </colorramp>
          <item alpha="255" label="0" value="0" color="#2b83ba"/>
          <item alpha="255" label="0.025" value="0.025" color="#4f9db4"/>
          <item alpha="255" label="0.05" value="0.05" color="#74b7ae"/>
          <item alpha="255" label="0.075" value="0.075" color="#99d0a7"/>
          <item alpha="255" label="0.1" value="0.1" color="#b7e2a8"/>
          <item alpha="255" label="0.125" value="0.125" color="#cfecb0"/>
          <item alpha="255" label="0.15" value="0.15" color="#e7f6b8"/>
          <item alpha="255" label="0.175" value="0.175" color="#ffffbf"/>
          <item alpha="255" label="0.2" value="0.2" color="#ffe8a4"/>
          <item alpha="255" label="0.225" value="0.225" color="#fed189"/>
          <item alpha="255" label="0.25" value="0.25" color="#feba6e"/>
          <item alpha="255" label="0.275" value="0.275" color="#f89957"/>
          <item alpha="255" label="0.3" value="0.3" color="#ed6e43"/>
          <item alpha="255" label="0.325" value="0.325" color="#e2432f"/>
          <item alpha="255" label="0.35" value="0.35" color="#d7191c"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation saturation="0" colorizeGreen="128" colorizeBlue="128" colorizeRed="255" colorizeOn="0" colorizeStrength="100" grayscaleMode="0"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
