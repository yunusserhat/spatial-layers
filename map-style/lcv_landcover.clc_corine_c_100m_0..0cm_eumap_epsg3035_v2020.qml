
<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" version="3.10.4-A Coruña" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer band="1" opacity="1" type="paletted" alphaBand="-1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
      <paletteEntry color="#e6004d" label="111 - Continuous urban fabric" alpha="255" value="1"/>
        <paletteEntry color="#ff0000" label="112 - Discontinuous urban fabric" alpha="255" value="2"/>
        <paletteEntry color="#cc4df2" label="121 - Industrial or commercial units" alpha="255" value="3"/>
        <paletteEntry color="#cc0000" label="122 - Road and rail networks and associated land" alpha="255" value="4"/>
        <paletteEntry color="#e6cccc" label="123 - Port areas" alpha="255" value="5"/>
        <paletteEntry color="#e6cce6" label="124 - Airports" alpha="255" value="6"/>
        <paletteEntry color="#a600cc" label="131 - Mineral extraction sites" alpha="255" value="7"/>
        <paletteEntry color="#a64d00" label="132 - Dump sites" alpha="255" value="8"/>
        <paletteEntry color="#ff4dff" label="133 - Construction sites" alpha="255" value="9"/>
        <paletteEntry color="#ffa6ff" label="141 - Green urban areas" alpha="255" value="10"/>
        <paletteEntry color="#ffe6ff" label="142 - Sport and leisure facilities" alpha="255" value="11"/>
        <paletteEntry color="#ffffa8" label="211 - Non-irrigated arable land" alpha="255" value="12"/>
        <paletteEntry color="#ffff00" label="212 - Permanently irrigated land" alpha="255" value="13"/>
        <paletteEntry color="#e6e600" label="213 - Rice fields" alpha="255" value="14"/>
        <paletteEntry color="#e68000" label="221 - Vineyards" alpha="255" value="15"/>
        <paletteEntry color="#f2a64d" label="222 - Fruit trees and berry plantations" alpha="255" value="16"/>
        <paletteEntry color="#e6a600" label="223 - Olive groves" alpha="255" value="17"/>
        <paletteEntry color="#e6e64d" label="231 - Pastures" alpha="255" value="18"/>
        <paletteEntry color="#ffe6a6" label="241 - Annual crops associated with permanent crops" alpha="255" value="19"/>
        <paletteEntry color="#ffe64d" label="242 - Complex cultivation patterns" alpha="255" value="20"/>
        <paletteEntry color="#e6cc4d" label="243 - Land principally occupied by agriculture with significant areas of natural vegetation" alpha="255" value="21"/>
        <paletteEntry color="#f2cca6" label="244 - Agro-forestry areas" alpha="255" value="22"/>
        <paletteEntry color="#80ff00" label="311 - Broad-leaved forest" alpha="255" value="23"/>
        <paletteEntry color="#00a600" label="312 - Coniferous forest" alpha="255" value="24"/>
        <paletteEntry color="#4dff00" label="313 - Mixed forest" alpha="255" value="25"/>
        <paletteEntry color="#ccf24d" label="321 - Natural grasslands" alpha="255" value="26"/>
        <paletteEntry color="#a6ff80" label="322 - Moors and heathland" alpha="255" value="27"/>
        <paletteEntry color="#a6e64d" label="323 - Sclerophyllous vegetation" alpha="255" value="28"/>
        <paletteEntry color="#a6f200" label="324 - Transitional woodland-shrub" alpha="255" value="29"/>
        <paletteEntry color="#e6e6e6" label="331 - Beaches - dunes - sands" alpha="255" value="30"/>
        <paletteEntry color="#cccccc" label="332 - Bare rocks" alpha="255" value="31"/>
        <paletteEntry color="#ccffcc" label="333 - Sparsely vegetated areas" alpha="255" value="32"/>
        <paletteEntry color="#000000" label="334 - Burnt areas" alpha="255" value="33"/>
        <paletteEntry color="#a6e6cc" label="335 - Glaciers and perpetual snow" alpha="255" value="34"/>
        <paletteEntry color="#a6a6ff" label="411 - Inland marshes" alpha="255" value="35"/>
        <paletteEntry color="#4d4dff" label="412 - Peat bogs" alpha="255" value="36"/>
        <paletteEntry color="#ccccff" label="421 - Salt marshes" alpha="255" value="37"/>
        <paletteEntry color="#e6e6ff" label="422 - Salines" alpha="255" value="38"/>
        <paletteEntry color="#a6a6e6" label="423 - Intertidal flats" alpha="255" value="39"/>
        <paletteEntry color="#00ccf2" label="511 - Water courses" alpha="255" value="40"/>
        <paletteEntry color="#80f2e6" label="512 - Water bodies" alpha="255" value="41"/>
        <paletteEntry color="#00ffa6" label="521 - Coastal lagoons" alpha="255" value="42"/>
        <paletteEntry color="#a6ffe6" label="522 - Estuaries" alpha="255" value="43"/>
        <paletteEntry color="#e6f2ff" label="523 - Sea and ocean" alpha="255" value="44"/>
        <paletteEntry color="#ffffff" label="999 - NODATA" alpha="255" value="48"/>
         
      </colorPalette>
      <colorramp name="[source]" type="randomcolors"/>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeRed="255" colorizeOn="0" colorizeBlue="128" saturation="0" grayscaleMode="0" colorizeGreen="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
