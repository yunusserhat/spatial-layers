<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.clc_corine_c_100m_0..0cm_2000_eumap_epsg3035_v2020</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#e6004d" label="111 - Continuous urban fabric" quantity="1"/>
              <sld:ColorMapEntry color="#ff0000" label="112 - Discontinuous urban fabric" quantity="2"/>
              <sld:ColorMapEntry color="#cc4df2" label="121 - Industrial or commercial units" quantity="3"/>
              <sld:ColorMapEntry color="#cc0000" label="122 - Road and rail networks and associated land" quantity="4"/>
              <sld:ColorMapEntry color="#e6cccc" label="123 - Port areas" quantity="5"/>
              <sld:ColorMapEntry color="#e6cce6" label="124 - Airports" quantity="6"/>
              <sld:ColorMapEntry color="#a600cc" label="131 - Mineral extraction sites" quantity="7"/>
              <sld:ColorMapEntry color="#a64d00" label="132 - Dump sites" quantity="8"/>
              <sld:ColorMapEntry color="#ff4dff" label="133 - Construction sites" quantity="9"/>
              <sld:ColorMapEntry color="#ffa6ff" label="141 - Green urban areas" quantity="10"/>
              <sld:ColorMapEntry color="#ffe6ff" label="142 - Sport and leisure facilities" quantity="11"/>
              <sld:ColorMapEntry color="#ffffa8" label="211 - Non-irrigated arable land" quantity="12"/>
              <sld:ColorMapEntry color="#ffff00" label="212 - Permanently irrigated land" quantity="13"/>
              <sld:ColorMapEntry color="#e6e600" label="213 - Rice fields" quantity="14"/>
              <sld:ColorMapEntry color="#e68000" label="221 - Vineyards" quantity="15"/>
              <sld:ColorMapEntry color="#f2a64d" label="222 - Fruit trees and berry plantations" quantity="16"/>
              <sld:ColorMapEntry color="#e6a600" label="223 - Olive groves" quantity="17"/>
              <sld:ColorMapEntry color="#e6e64d" label="231 - Pastures" quantity="18"/>
              <sld:ColorMapEntry color="#ffe6a6" label="241 - Annual crops associated with permanent crops" quantity="19"/>
              <sld:ColorMapEntry color="#ffe64d" label="242 - Complex cultivation patterns" quantity="20"/>
              <sld:ColorMapEntry color="#e6cc4d" label="243 - Land principally occupied by agriculture with significant areas of natural vegetation" quantity="21"/>
              <sld:ColorMapEntry color="#f2cca6" label="244 - Agro-forestry areas" quantity="22"/>
              <sld:ColorMapEntry color="#80ff00" label="311 - Broad-leaved forest" quantity="23"/>
              <sld:ColorMapEntry color="#00a600" label="312 - Coniferous forest" quantity="24"/>
              <sld:ColorMapEntry color="#4dff00" label="313 - Mixed forest" quantity="25"/>
              <sld:ColorMapEntry color="#ccf24d" label="321 - Natural grasslands" quantity="26"/>
              <sld:ColorMapEntry color="#a6ff80" label="322 - Moors and heathland" quantity="27"/>
              <sld:ColorMapEntry color="#a6e64d" label="323 - Sclerophyllous vegetation" quantity="28"/>
              <sld:ColorMapEntry color="#a6f200" label="324 - Transitional woodland-shrub" quantity="29"/>
              <sld:ColorMapEntry color="#e6e6e6" label="331 - Beaches - dunes - sands" quantity="30"/>
              <sld:ColorMapEntry color="#cccccc" label="332 - Bare rocks" quantity="31"/>
              <sld:ColorMapEntry color="#ccffcc" label="333 - Sparsely vegetated areas" quantity="32"/>
              <sld:ColorMapEntry color="#000000" label="334 - Burnt areas" quantity="33"/>
              <sld:ColorMapEntry color="#a6e6cc" label="335 - Glaciers and perpetual snow" quantity="34"/>
              <sld:ColorMapEntry color="#a6a6ff" label="411 - Inland marshes" quantity="35"/>
              <sld:ColorMapEntry color="#4d4dff" label="412 - Peat bogs" quantity="36"/>
              <sld:ColorMapEntry color="#ccccff" label="421 - Salt marshes" quantity="37"/>
              <sld:ColorMapEntry color="#e6e6ff" label="422 - Salines" quantity="38"/>
              <sld:ColorMapEntry color="#a6a6e6" label="423 - Intertidal flats" quantity="39"/>
              <sld:ColorMapEntry color="#00ccf2" label="511 - Water courses" quantity="40"/>
              <sld:ColorMapEntry color="#80f2e6" label="512 - Water bodies" quantity="41"/>
              <sld:ColorMapEntry color="#00ffa6" label="521 - Coastal lagoons" quantity="42"/>
              <sld:ColorMapEntry color="#a6ffe6" label="522 - Estuaries" quantity="43"/>
              <sld:ColorMapEntry color="#e6f2ff" label="523 - Sea and ocean" quantity="44"/>
              <sld:ColorMapEntry color="#ffffff" label="999 - NODATA" quantity="48"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
