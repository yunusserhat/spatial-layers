<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#fcfdbf"/>
        <prop k="preset_color_name_0" v="0"/>
        <prop k="preset_color_1" v="#fcf4b6"/>
        <prop k="preset_color_name_1" v="2"/>
        <prop k="preset_color_2" v="#fdebac"/>
        <prop k="preset_color_name_2" v="5"/>
        <prop k="preset_color_3" v="#fde2a3"/>
        <prop k="preset_color_name_3" v="7"/>
        <prop k="preset_color_4" v="#fed89a"/>
        <prop k="preset_color_name_4" v="9"/>
        <prop k="preset_color_5" v="#fecf92"/>
        <prop k="preset_color_name_5" v="12"/>
        <prop k="preset_color_6" v="#fec68a"/>
        <prop k="preset_color_name_6" v="14"/>
        <prop k="preset_color_7" v="#febd82"/>
        <prop k="preset_color_name_7" v="16"/>
        <prop k="preset_color_8" v="#feb47b"/>
        <prop k="preset_color_name_8" v="19"/>
        <prop k="preset_color_9" v="#feaa74"/>
        <prop k="preset_color_name_9" v="21"/>
        <prop k="preset_color_10" v="#fea16e"/>
        <prop k="preset_color_name_10" v="24"/>
        <prop k="preset_color_11" v="#fd9869"/>
        <prop k="preset_color_name_11" v="26"/>
        <prop k="preset_color_12" v="#fc8e64"/>
        <prop k="preset_color_name_12" v="28"/>
        <prop k="preset_color_13" v="#fb8560"/>
        <prop k="preset_color_name_13" v="31"/>
        <prop k="preset_color_14" v="#f97b5d"/>
        <prop k="preset_color_name_14" v="33"/>
        <prop k="preset_color_15" v="#f7725c"/>
        <prop k="preset_color_name_15" v="35"/>
        <prop k="preset_color_16" v="#f4695c"/>
        <prop k="preset_color_name_16" v="38"/>
        <prop k="preset_color_17" v="#f1605d"/>
        <prop k="preset_color_name_17" v="40"/>
        <prop k="preset_color_18" v="#ec5860"/>
        <prop k="preset_color_name_18" v="42"/>
        <prop k="preset_color_19" v="#e75263"/>
        <prop k="preset_color_name_19" v="45"/>
        <prop k="preset_color_20" v="#e04c67"/>
        <prop k="preset_color_name_20" v="47"/>
        <prop k="preset_color_21" v="#d9466b"/>
        <prop k="preset_color_name_21" v="49"/>
        <prop k="preset_color_22" v="#d2426f"/>
        <prop k="preset_color_name_22" v="52"/>
        <prop k="preset_color_23" v="#ca3e72"/>
        <prop k="preset_color_name_23" v="54"/>
        <prop k="preset_color_24" v="#c23b75"/>
        <prop k="preset_color_name_24" v="56"/>
        <prop k="preset_color_25" v="#ba3878"/>
        <prop k="preset_color_name_25" v="59"/>
        <prop k="preset_color_26" v="#b2357b"/>
        <prop k="preset_color_name_26" v="61"/>
        <prop k="preset_color_27" v="#aa337d"/>
        <prop k="preset_color_name_27" v="64"/>
        <prop k="preset_color_28" v="#a1307e"/>
        <prop k="preset_color_name_28" v="66"/>
        <prop k="preset_color_29" v="#992d80"/>
        <prop k="preset_color_name_29" v="68"/>
        <prop k="preset_color_30" v="#912b81"/>
        <prop k="preset_color_name_30" v="71"/>
        <prop k="preset_color_31" v="#892881"/>
        <prop k="preset_color_name_31" v="73"/>
        <prop k="preset_color_32" v="#812581"/>
        <prop k="preset_color_name_32" v="75"/>
        <prop k="preset_color_33" v="#792282"/>
        <prop k="preset_color_name_33" v="78"/>
        <prop k="preset_color_34" v="#721f81"/>
        <prop k="preset_color_name_34" v="80"/>
        <prop k="preset_color_35" v="#6a1c81"/>
        <prop k="preset_color_name_35" v="82"/>
        <prop k="preset_color_36" v="#621980"/>
        <prop k="preset_color_name_36" v="85"/>
        <prop k="preset_color_37" v="#5a167e"/>
        <prop k="preset_color_name_37" v="87"/>
        <prop k="preset_color_38" v="#52137c"/>
        <prop k="preset_color_name_38" v="89"/>
        <prop k="preset_color_39" v="#4a1079"/>
        <prop k="preset_color_name_39" v="92"/>
        <prop k="preset_color_40" v="#420f75"/>
        <prop k="preset_color_name_40" v="94"/>
        <prop k="preset_color_41" v="#390f6e"/>
        <prop k="preset_color_name_41" v="96"/>
        <prop k="preset_color_42" v="#311165"/>
        <prop k="preset_color_name_42" v="99"/>
        <prop k="preset_color_43" v="#29115a"/>
        <prop k="preset_color_name_43" v="101"/>
        <prop k="preset_color_44" v="#21114e"/>
        <prop k="preset_color_name_44" v="104"/>
        <prop k="preset_color_45" v="#1a1042"/>
        <prop k="preset_color_name_45" v="106"/>
        <prop k="preset_color_46" v="#140e36"/>
        <prop k="preset_color_name_46" v="108"/>
        <prop k="preset_color_47" v="#0e0b2b"/>
        <prop k="preset_color_name_47" v="111"/>
        <prop k="preset_color_48" v="#090720"/>
        <prop k="preset_color_name_48" v="113"/>
        <prop k="preset_color_49" v="#050416"/>
        <prop k="preset_color_name_49" v="115"/>
        <prop k="preset_color_50" v="#02020b"/>
        <prop k="preset_color_name_50" v="118"/>
        <prop k="preset_color_51" v="#000004"/>
        <prop k="preset_color_name_51" v="120"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="0" color="#fcfdbf" value="0"/>
        <item alpha="255" label="2" color="#fcf4b6" value="2.352959999999995"/>
        <item alpha="255" label="5" color="#fdebac" value="4.705920000000003"/>
        <item alpha="255" label="7" color="#fde2a3" value="7.0588799999999985"/>
        <item alpha="255" label="9" color="#fed89a" value="9.411720000000003"/>
        <item alpha="255" label="12" color="#fecf92" value="11.764679999999998"/>
        <item alpha="255" label="14" color="#fec68a" value="14.117639999999994"/>
        <item alpha="255" label="16" color="#febd82" value="16.4706"/>
        <item alpha="255" label="19" color="#feb47b" value="18.823559999999997"/>
        <item alpha="255" label="21" color="#feaa74" value="21.176520000000004"/>
        <item alpha="255" label="24" color="#fea16e" value="23.529359999999997"/>
        <item alpha="255" label="26" color="#fd9869" value="25.882320000000007"/>
        <item alpha="255" label="28" color="#fc8e64" value="28.23528"/>
        <item alpha="255" label="31" color="#fb8560" value="30.588239999999995"/>
        <item alpha="255" label="33" color="#f97b5d" value="32.9412"/>
        <item alpha="255" label="35" color="#f7725c" value="35.29416"/>
        <item alpha="255" label="38" color="#f4695c" value="37.647000000000006"/>
        <item alpha="255" label="40" color="#f1605d" value="39.99996"/>
        <item alpha="255" label="42" color="#ec5860" value="42.35292"/>
        <item alpha="255" label="45" color="#e75263" value="44.70588"/>
        <item alpha="255" label="47" color="#e04c67" value="47.05884"/>
        <item alpha="255" label="49" color="#d9466b" value="49.41180000000001"/>
        <item alpha="255" label="52" color="#d2426f" value="51.76476"/>
        <item alpha="255" label="54" color="#ca3e72" value="54.1176"/>
        <item alpha="255" label="56" color="#c23b75" value="56.47056"/>
        <item alpha="255" label="59" color="#ba3878" value="58.823519999999995"/>
        <item alpha="255" label="61" color="#b2357b" value="61.17647999999999"/>
        <item alpha="255" label="64" color="#aa337d" value="63.52944"/>
        <item alpha="255" label="66" color="#a1307e" value="65.8824"/>
        <item alpha="255" label="68" color="#992d80" value="68.23524"/>
        <item alpha="255" label="71" color="#912b81" value="70.5882"/>
        <item alpha="255" label="73" color="#892881" value="72.94116"/>
        <item alpha="255" label="75" color="#812581" value="75.29411999999999"/>
        <item alpha="255" label="78" color="#792282" value="77.64708"/>
        <item alpha="255" label="80" color="#721f81" value="80.00004"/>
        <item alpha="255" label="82" color="#6a1c81" value="82.353"/>
        <item alpha="255" label="85" color="#621980" value="84.70584"/>
        <item alpha="255" label="87" color="#5a167e" value="87.05879999999999"/>
        <item alpha="255" label="89" color="#52137c" value="89.41176"/>
        <item alpha="255" label="92" color="#4a1079" value="91.76472"/>
        <item alpha="255" label="94" color="#420f75" value="94.11768"/>
        <item alpha="255" label="96" color="#390f6e" value="96.47064"/>
        <item alpha="255" label="99" color="#311165" value="98.82347999999999"/>
        <item alpha="255" label="101" color="#29115a" value="101.17644"/>
        <item alpha="255" label="104" color="#21114e" value="103.5294"/>
        <item alpha="255" label="106" color="#1a1042" value="105.88235999999999"/>
        <item alpha="255" label="108" color="#140e36" value="108.235296"/>
        <item alpha="255" label="111" color="#0e0b2b" value="110.58823199999999"/>
        <item alpha="255" label="113" color="#090720" value="112.94118"/>
        <item alpha="255" label="115" color="#050416" value="115.294116"/>
        <item alpha="255" label="118" color="#02020b" value="117.647064"/>
        <item alpha="255" label="120" color="#000004" value="120"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

