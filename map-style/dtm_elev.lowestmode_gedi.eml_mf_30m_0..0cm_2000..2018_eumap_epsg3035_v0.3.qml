<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#00bfbf"/>
        <prop k="preset_color_name_0" v="-150"/>
        <prop k="preset_color_1" v="#00ff00"/>
        <prop k="preset_color_name_1" v="4680"/>
        <prop k="preset_color_2" v="#ffff00"/>
        <prop k="preset_color_name_2" v="9510"/>
        <prop k="preset_color_3" v="#ff7f00"/>
        <prop k="preset_color_name_3" v="14340"/>
        <prop k="preset_color_4" v="#bf7f3f"/>
        <prop k="preset_color_name_4" v="19170"/>
        <prop k="preset_color_5" v="#141414"/>
        <prop k="preset_color_name_5" v="24000"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="-150" color="#00bfbf" value="-150"/>
        <item alpha="255" label="4680" color="#00ff00" value="4680"/>
        <item alpha="255" label="9510" color="#ffff00" value="9510"/>
        <item alpha="255" label="14340" color="#ff7f00" value="14340"/>
        <item alpha="255" label="19170" color="#bf7f3f" value="19170"/>
        <item alpha="255" label="24000" color="#141414" value="24000"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

