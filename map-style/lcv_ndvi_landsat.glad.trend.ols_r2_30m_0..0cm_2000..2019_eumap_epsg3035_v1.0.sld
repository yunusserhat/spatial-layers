<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_ndvi_landsat.glad.ard_trend.ols.r2_30m_0..0cm_2000..2019_eumap_epsg3035_v1.0.tif</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry quantity="0" color="#d01c8b" label="&lt; 0.15"/>
              <sld:ColorMapEntry quantity="150" color="#f1b6da" label="0.15 - 0.30"/>
              <sld:ColorMapEntry quantity="300" color="#f7f7f7" label="0.30 - 0.45"/>
              <sld:ColorMapEntry quantity="450" color="#b8e186" label="0.45 - 0.60"/>
              <sld:ColorMapEntry quantity="600" color="#4dac26" label="> 0.60"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
