<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#163db0"/>
        <prop k="preset_color_name_0" v="&lt;= 10"/>
        <prop k="preset_color_1" v="#176596"/>
        <prop k="preset_color_name_1" v="10 - 13"/>
        <prop k="preset_color_2" v="#1f8f8d"/>
        <prop k="preset_color_name_2" v="13 - 16"/>
        <prop k="preset_color_3" v="#23ba99"/>
        <prop k="preset_color_name_3" v="16 - 19"/>
        <prop k="preset_color_4" v="#abdda4"/>
        <prop k="preset_color_name_4" v="19 - 22"/>
        <prop k="preset_color_5" v="#f4c379"/>
        <prop k="preset_color_name_5" v="22 - 25"/>
        <prop k="preset_color_6" v="#ff805d"/>
        <prop k="preset_color_name_6" v="25 - 28"/>
        <prop k="preset_color_7" v="#f65737"/>
        <prop k="preset_color_name_7" v="28 - 31"/>
        <prop k="preset_color_8" v="#c94839"/>
        <prop k="preset_color_name_8" v="31 - 34"/>
        <prop k="preset_color_9" v="#a91315"/>
        <prop k="preset_color_name_9" v="> 34"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="&lt;= 10" color="#163db0" value="10"/>
        <item alpha="255" label="10 - 13" color="#176596" value="13"/>
        <item alpha="255" label="13 - 16" color="#1f8f8d" value="16"/>
        <item alpha="255" label="16 - 19" color="#23ba99" value="19"/>
        <item alpha="255" label="19 - 22" color="#abdda4" value="22"/>
        <item alpha="255" label="22 - 25" color="#f4c379" value="25"/>
        <item alpha="255" label="25 - 28" color="#ff805d" value="28"/>
        <item alpha="255" label="28 - 31" color="#f65737" value="31"/>
        <item alpha="255" label="31 - 34" color="#c94839" value="34"/>
        <item alpha="255" label="> 34" color="#a91315" value="inf"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

