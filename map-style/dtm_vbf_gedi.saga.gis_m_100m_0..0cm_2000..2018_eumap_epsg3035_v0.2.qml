<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#faffff"/>
        <prop k="preset_color_name_0" v="2"/>
        <prop k="preset_color_1" v="#d3fad3"/>
        <prop k="preset_color_name_1" v="64"/>
        <prop k="preset_color_2" v="#acf5a8"/>
        <prop k="preset_color_name_2" v="126"/>
        <prop k="preset_color_3" v="#56c5b8"/>
        <prop k="preset_color_name_3" v="188"/>
        <prop k="preset_color_4" v="#0096c8"/>
        <prop k="preset_color_name_4" v="250"/>
        <prop k="preset_color_5" v="#00507d"/>
        <prop k="preset_color_name_5" v="311"/>
        <prop k="preset_color_6" v="#000a32"/>
        <prop k="preset_color_name_6" v="373"/>
        <prop k="preset_color_7" v="#000519"/>
        <prop k="preset_color_name_7" v="435"/>
        <prop k="preset_color_8" v="#074882"/>
        <prop k="preset_color_name_8" v="497"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="2" color="#faffff" value="2"/>
        <item alpha="255" label="64" color="#d3fad3" value="63.875"/>
        <item alpha="255" label="126" color="#acf5a8" value="125.75"/>
        <item alpha="255" label="188" color="#56c5b8" value="187.625"/>
        <item alpha="255" label="250" color="#0096c8" value="249.5"/>
        <item alpha="255" label="311" color="#00507d" value="311.375"/>
        <item alpha="255" label="373" color="#000a32" value="373.25"/>
        <item alpha="255" label="435" color="#000519" value="435.125"/>
        <item alpha="255" label="497" color="#074882" value="497"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

