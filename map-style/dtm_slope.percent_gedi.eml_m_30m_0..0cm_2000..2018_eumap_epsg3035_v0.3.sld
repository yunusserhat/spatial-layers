<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_slope.percent_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3</sld:Name>
            <sld:Description>Slope in percent based on DTM in dm</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#fef0d9" label="0" opacity="1.0" quantity="0"/>
                            <sld:ColorMapEntry color="#fdd49e" label="217" opacity="1.0" quantity="216.70999999999998"/>
                            <sld:ColorMapEntry color="#fdbb84" label="433" opacity="1.0" quantity="433.28999999999996"/>
                            <sld:ColorMapEntry color="#fc8d59" label="650" opacity="1.0" quantity="650"/>
                            <sld:ColorMapEntry color="#e34a33" label="867" opacity="1.0" quantity="866.7099999999999"/>
                            <sld:ColorMapEntry color="#b30000" label="1083" opacity="1.0" quantity="1083.29"/>
                            <sld:ColorMapEntry color="#830000" label="1300" opacity="1.0" quantity="1300"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
