<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_vbf_gedi.saga.gis_m_100m_0..0cm_2000..2018_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>Digital Terrain Model MrVBF index</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#faffff" label="2" opacity="1.0" quantity="2"/>
                            <sld:ColorMapEntry color="#d3fad3" label="64" opacity="1.0" quantity="63.875"/>
                            <sld:ColorMapEntry color="#acf5a8" label="126" opacity="1.0" quantity="125.75"/>
                            <sld:ColorMapEntry color="#56c5b8" label="188" opacity="1.0" quantity="187.625"/>
                            <sld:ColorMapEntry color="#0096c8" label="250" opacity="1.0" quantity="249.5"/>
                            <sld:ColorMapEntry color="#00507d" label="311" opacity="1.0" quantity="311.375"/>
                            <sld:ColorMapEntry color="#000a32" label="373" opacity="1.0" quantity="373.25"/>
                            <sld:ColorMapEntry color="#000519" label="435" opacity="1.0" quantity="435.125"/>
                            <sld:ColorMapEntry color="#074882" label="497" opacity="1.0" quantity="497"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
