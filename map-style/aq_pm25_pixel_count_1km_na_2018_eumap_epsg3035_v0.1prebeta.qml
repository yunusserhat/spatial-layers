<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#f7fbff"/>
        <prop k="preset_color_name_0" v="0"/>
        <prop k="preset_color_1" v="#deebf7"/>
        <prop k="preset_color_name_1" v="4"/>
        <prop k="preset_color_2" v="#c6dbef"/>
        <prop k="preset_color_name_2" v="8"/>
        <prop k="preset_color_3" v="#9ecae1"/>
        <prop k="preset_color_name_3" v="12"/>
        <prop k="preset_color_4" v="#6baed6"/>
        <prop k="preset_color_name_4" v="16"/>
        <prop k="preset_color_5" v="#4292c6"/>
        <prop k="preset_color_name_5" v="20"/>
        <prop k="preset_color_6" v="#2171b5"/>
        <prop k="preset_color_name_6" v="24"/>
        <prop k="preset_color_7" v="#08519c"/>
        <prop k="preset_color_name_7" v="28"/>
        <prop k="preset_color_8" v="#08306b"/>
        <prop k="preset_color_name_8" v="31"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="0" color="#f7fbff" value="0"/>
        <item alpha="255" label="4" color="#deebf7" value="4"/>
        <item alpha="255" label="8" color="#c6dbef" value="8"/>
        <item alpha="255" label="12" color="#9ecae1" value="12"/>
        <item alpha="255" label="16" color="#6baed6" value="16"/>
        <item alpha="255" label="20" color="#4292c6" value="20"/>
        <item alpha="255" label="24" color="#2171b5" value="24"/>
        <item alpha="255" label="28" color="#08519c" value="28"/>
        <item alpha="255" label="31" color="#08306b" value="31"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

