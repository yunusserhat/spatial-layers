<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:gml="http://www.opengis.net/gml" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv.change.c_30m_0..0cm_2000-2001_eumap_epsg3035_v0.1.tif</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#f294fb" label="Crop expansion" quantity="1"/>
              <sld:ColorMapEntry color="#a7562c" label="Deforestation" quantity="2"/>
              <sld:ColorMapEntry color="#965591" label="deforestation &amp; crop expansion" quantity="3"/>
              <sld:ColorMapEntry color="#cb0809" label="deforestation &amp; urbanization" quantity="4"/>
              <sld:ColorMapEntry color="#c7c7c7" label="Desertification" quantity="5"/>
              <sld:ColorMapEntry color="#ffba3b" label="land abondonment" quantity="6"/>
              <sld:ColorMapEntry color="#d1ad67" label="land abondonment &amp; desertification" quantity="7"/>
              <sld:ColorMapEntry color="#6e6e6e" label="Other" quantity="8"/>
              <sld:ColorMapEntry color="#008626" label="Reforestation" quantity="9"/>
              <sld:ColorMapEntry color="#ff0d0e" label="Urbanization" quantity="10"/>
              <sld:ColorMapEntry color="#0033c3" label="Water expansion" quantity="11"/>
              <sld:ColorMapEntry color="#ff6565" label="water reduction" quantity="12"/>
              <sld:ColorMapEntry color="#009082" label="wetland degradation" quantity="16"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
