#!/bin/bash

IFS=$'\n'

sld_file=$1
qml_file=$(echo $sld_file | sed s/.sld/.qml/g)

qml_pallete_entry=""
for cme in $(cat $sld_file | grep sld:ColorMapEntry); do
	color=$(echo $cme | grep sld:ColorMapEntry | egrep -o 'color=".*"' | cut -d\" -f2)
	label=$(echo $cme | grep sld:ColorMapEntry | egrep -o 'label=".*"' | cut -d\" -f2)
	value=$(echo $cme | grep sld:ColorMapEntry | egrep -o 'quantity=".*"' | cut -d\" -f2)
	new_entry="<paletteEntry color=\"$color\" label=\"$label\" alpha=\"255\" value=\"$value\"/>"
	qml_pallete_entry="${qml_pallete_entry}$new_entry\n        "
done

echo "Saving $qml_file"
echo -e "
<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale=\"1e+08\" version=\"3.10.4-A Coruña\" hasScaleBasedVisibilityFlag=\"0\" styleCategories=\"AllStyleCategories\" maxScale=\"0\">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key=\"WMSBackgroundLayer\" value=\"false\"/>
    <property key=\"WMSPublishDataSourceUrl\" value=\"false\"/>
    <property key=\"embeddedWidgets/count\" value=\"0\"/>
    <property key=\"identify/format\" value=\"Value\"/>
  </customproperties>
  <pipe>
    <rasterrenderer band=\"1\" opacity=\"1\" type=\"paletted\" alphaBand=\"-1\">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
      $qml_pallete_entry 
      </colorPalette>
      <colorramp name=\"[source]\" type=\"randomcolors\"/>
    </rasterrenderer>
    <brightnesscontrast contrast=\"0\" brightness=\"0\"/>
    <huesaturation colorizeRed=\"255\" colorizeOn=\"0\" colorizeBlue=\"128\" saturation=\"0\" grayscaleMode=\"0\" colorizeGreen=\"128\" colorizeStrength=\"100\"/>
    <rasterresampler maxOversampling=\"3\" zoomedOutResampler=\"bilinear\"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>" &> $qml_file