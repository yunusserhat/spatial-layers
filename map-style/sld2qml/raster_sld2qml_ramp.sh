#!/bin/bash

IFS=$'\n'

sld_file=$1
qml_file=$(echo $sld_file | sed s/.sld/.qml/g)

qml_color_preset=""
i=0
for cme in $(cat $sld_file | grep sld:ColorMapEntry); do
  color=$(echo $cme | egrep -o 'color=".*"' | cut -d\" -f2)
  label=$(echo $cme | egrep -o 'label=".*"' | cut -d\" -f2)
  value=$(echo $cme | egrep -o 'quantity=".*"' | cut -d\" -f2)
	new_preset="<prop k=\"preset_color_$i\" v=\"$color\"/>\n        <prop k=\"preset_color_name_$i\" v=\"$label\"/>"
	new_entry="<item alpha=\"255\" label=\"$label\" color=\"$color\" value=\"$value\"/>"
	
	qml_color_preset="${qml_color_preset}$new_preset\n        "
	qml_pallete_entry="${qml_pallete_entry}$new_entry\n        "

	i=$(($i+1))
done

echo "Saving $qml_file"
echo -e "<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale=\"0\" minScale=\"1e+08\" version=\"3.10.4-A Coruña\" styleCategories=\"AllStyleCategories\" hasScaleBasedVisibilityFlag=\"0\">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key=\"WMSBackgroundLayer\" value=\"false\"/>
    <property key=\"WMSPublishDataSourceUrl\" value=\"false\"/>
    <property key=\"embeddedWidgets/count\" value=\"0\"/>
    <property key=\"identify/format\" value=\"Value\"/>
  </customproperties>
  <pipe>
    <rasterrenderer type=\"singlebandpseudocolor\" alphaBand=\"-1\" band=\"1\" classificationMin=\"2\" opacity=\"1\" classificationMax=\"19\">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType=\"INTERPOLATED\" classificationMode=\"2\" clip=\"0\">
      <colorramp type=\"preset\" name=\"[source]\">
      	$qml_color_preset
        <prop k=\"rampType\" v=\"preset\"/>
      </colorramp>
      $qml_pallete_entry 
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast=\"0\" brightness=\"0\"/>
    <huesaturation colorizeGreen=\"128\" colorizeOn=\"0\" colorizeRed=\"255\" colorizeStrength=\"100\" colorizeBlue=\"128\" saturation=\"0\" grayscaleMode=\"0\"/>
    <rasterresampler maxOversampling=\"3\" zoomedOutResampler=\"bilinear\"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
" &> $qml_file