#!/bin/bash

for sld in $(find ../ -name *.sld); do
	qml=$(echo $sld | sed s/.sld/.qml/g)
	if [ -e $qml ]; then
		echo "The $qml file already exists"
	else
		if [ -n "$(cat $sld | grep type=\"values\")" ]; then
			./raster_sld2qml_values.sh $sld
		else
			./raster_sld2qml_ramp.sh $sld
		fi 
	fi

done