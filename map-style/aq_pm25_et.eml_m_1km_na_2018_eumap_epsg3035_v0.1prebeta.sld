<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>map1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="intervals">
              <sld:ColorMapEntry color="#163db0" label="&lt;= 10" quantity="10"/>
              <sld:ColorMapEntry color="#176596" label="10 - 13" quantity="13"/>
              <sld:ColorMapEntry color="#1f8f8d" label="13 - 16" quantity="16"/>
              <sld:ColorMapEntry color="#23ba99" label="16 - 19" quantity="19"/>
              <sld:ColorMapEntry color="#abdda4" label="19 - 22" quantity="22"/>
              <sld:ColorMapEntry color="#f4c379" label="22 - 25" quantity="25"/>
              <sld:ColorMapEntry color="#ff805d" label="25 - 28" quantity="28"/>
              <sld:ColorMapEntry color="#f65737" label="28 - 31" quantity="31"/>
              <sld:ColorMapEntry color="#c94839" label="31 - 34" quantity="34"/>
              <sld:ColorMapEntry color="#a91315" label="> 34" quantity="inf"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
