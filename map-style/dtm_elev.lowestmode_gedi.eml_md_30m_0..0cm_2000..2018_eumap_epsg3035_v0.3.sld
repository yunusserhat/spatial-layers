<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_elev.lowestmode_gedi.eml_md_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3</sld:Name>
            <sld:Description>DTM error in dm</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#fcfdbf" label="0" opacity="1.0" quantity="0"/>
                            <sld:ColorMapEntry color="#fcf4b6" label="2" opacity="1.0" quantity="2.352959999999995"/>
                            <sld:ColorMapEntry color="#fdebac" label="5" opacity="1.0" quantity="4.705920000000003"/>
                            <sld:ColorMapEntry color="#fde2a3" label="7" opacity="1.0" quantity="7.0588799999999985"/>
                            <sld:ColorMapEntry color="#fed89a" label="9" opacity="1.0" quantity="9.411720000000003"/>
                            <sld:ColorMapEntry color="#fecf92" label="12" opacity="1.0" quantity="11.764679999999998"/>
                            <sld:ColorMapEntry color="#fec68a" label="14" opacity="1.0" quantity="14.117639999999994"/>
                            <sld:ColorMapEntry color="#febd82" label="16" opacity="1.0" quantity="16.4706"/>
                            <sld:ColorMapEntry color="#feb47b" label="19" opacity="1.0" quantity="18.823559999999997"/>
                            <sld:ColorMapEntry color="#feaa74" label="21" opacity="1.0" quantity="21.176520000000004"/>
                            <sld:ColorMapEntry color="#fea16e" label="24" opacity="1.0" quantity="23.529359999999997"/>
                            <sld:ColorMapEntry color="#fd9869" label="26" opacity="1.0" quantity="25.882320000000007"/>
                            <sld:ColorMapEntry color="#fc8e64" label="28" opacity="1.0" quantity="28.23528"/>
                            <sld:ColorMapEntry color="#fb8560" label="31" opacity="1.0" quantity="30.588239999999995"/>
                            <sld:ColorMapEntry color="#f97b5d" label="33" opacity="1.0" quantity="32.9412"/>
                            <sld:ColorMapEntry color="#f7725c" label="35" opacity="1.0" quantity="35.29416"/>
                            <sld:ColorMapEntry color="#f4695c" label="38" opacity="1.0" quantity="37.647000000000006"/>
                            <sld:ColorMapEntry color="#f1605d" label="40" opacity="1.0" quantity="39.99996"/>
                            <sld:ColorMapEntry color="#ec5860" label="42" opacity="1.0" quantity="42.35292"/>
                            <sld:ColorMapEntry color="#e75263" label="45" opacity="1.0" quantity="44.70588"/>
                            <sld:ColorMapEntry color="#e04c67" label="47" opacity="1.0" quantity="47.05884"/>
                            <sld:ColorMapEntry color="#d9466b" label="49" opacity="1.0" quantity="49.41180000000001"/>
                            <sld:ColorMapEntry color="#d2426f" label="52" opacity="1.0" quantity="51.76476"/>
                            <sld:ColorMapEntry color="#ca3e72" label="54" opacity="1.0" quantity="54.1176"/>
                            <sld:ColorMapEntry color="#c23b75" label="56" opacity="1.0" quantity="56.47056"/>
                            <sld:ColorMapEntry color="#ba3878" label="59" opacity="1.0" quantity="58.823519999999995"/>
                            <sld:ColorMapEntry color="#b2357b" label="61" opacity="1.0" quantity="61.17647999999999"/>
                            <sld:ColorMapEntry color="#aa337d" label="64" opacity="1.0" quantity="63.52944"/>
                            <sld:ColorMapEntry color="#a1307e" label="66" opacity="1.0" quantity="65.8824"/>
                            <sld:ColorMapEntry color="#992d80" label="68" opacity="1.0" quantity="68.23524"/>
                            <sld:ColorMapEntry color="#912b81" label="71" opacity="1.0" quantity="70.5882"/>
                            <sld:ColorMapEntry color="#892881" label="73" opacity="1.0" quantity="72.94116"/>
                            <sld:ColorMapEntry color="#812581" label="75" opacity="1.0" quantity="75.29411999999999"/>
                            <sld:ColorMapEntry color="#792282" label="78" opacity="1.0" quantity="77.64708"/>
                            <sld:ColorMapEntry color="#721f81" label="80" opacity="1.0" quantity="80.00004"/>
                            <sld:ColorMapEntry color="#6a1c81" label="82" opacity="1.0" quantity="82.353"/>
                            <sld:ColorMapEntry color="#621980" label="85" opacity="1.0" quantity="84.70584"/>
                            <sld:ColorMapEntry color="#5a167e" label="87" opacity="1.0" quantity="87.05879999999999"/>
                            <sld:ColorMapEntry color="#52137c" label="89" opacity="1.0" quantity="89.41176"/>
                            <sld:ColorMapEntry color="#4a1079" label="92" opacity="1.0" quantity="91.76472"/>
                            <sld:ColorMapEntry color="#420f75" label="94" opacity="1.0" quantity="94.11768"/>
                            <sld:ColorMapEntry color="#390f6e" label="96" opacity="1.0" quantity="96.47064"/>
                            <sld:ColorMapEntry color="#311165" label="99" opacity="1.0" quantity="98.82347999999999"/>
                            <sld:ColorMapEntry color="#29115a" label="101" opacity="1.0" quantity="101.17644"/>
                            <sld:ColorMapEntry color="#21114e" label="104" opacity="1.0" quantity="103.5294"/>
                            <sld:ColorMapEntry color="#1a1042" label="106" opacity="1.0" quantity="105.88235999999999"/>
                            <sld:ColorMapEntry color="#140e36" label="108" opacity="1.0" quantity="108.235296"/>
                            <sld:ColorMapEntry color="#0e0b2b" label="111" opacity="1.0" quantity="110.58823199999999"/>
                            <sld:ColorMapEntry color="#090720" label="113" opacity="1.0" quantity="112.94118"/>
                            <sld:ColorMapEntry color="#050416" label="115" opacity="1.0" quantity="115.294116"/>
                            <sld:ColorMapEntry color="#02020b" label="118" opacity="1.0" quantity="117.647064"/>
                            <sld:ColorMapEntry color="#000004" label="120" opacity="1.0" quantity="120"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
